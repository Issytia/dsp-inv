using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace IssyInv
{
	[BepInPlugin("issy.inv", "IssyInv", "0.1"), BepInProcess("DSPGAME.exe")]
	public class IssyInv : BaseUnityPlugin
	{
		private static ConfigEntry<int> width;
		private static ConfigEntry<int> rows_base;
		private static int rows { get {
			int count = rows_base.Value;
			for (var i = 2301; i <= 2306; i++) {
				if (GameMain.history.TechUnlocked(i))
					count += i >= 2305 ? 2 : 1;
				else
					break;
			}
			return count;
		}}
		private static ConfigEntry<bool> fuel;

		public void Start()
		{
			Harmony.CreateAndPatchAll(typeof(IssyInv));

			width = Config.Bind("General", "width", 10, new ConfigDescription("Inventory width (vanilla: 10)", new AcceptableValueRange<int>(1, 20)));
			rows_base = Config.Bind("General", "rows_base", 4, new ConfigDescription("Inventory base rows (vanilla: 4)", new AcceptableValueRange<int>(1, 20)));
			fuel = Config.Bind("General", "fuel", false, "Shrink fuel inventory");

			if (fuel.Value) {
				//var group = GameObject.Find("UI Root/Overlay Canvas/In Game/Windows/Mecha Window/fuel-group");
				//group.transform.localPosition = new Vector3(50f, 0f, 0f);
				//var storage = GameObject.Find("UI Root/Overlay Canvas/In Game/Windows/Mecha Window/fuel-group/fuel-storage");
				//storage.GetComponent<RectTransform>().sizeDelta = new Vector2(216f, 312f);
				//var fuel = GameObject.Find("UI Root/Overlay Canvas/In Game/Windows/Mecha Window/fuel-group/fuel");
				//fuel.transform.localPosition = new Vector3(77f, 157f, 0f);
			}
		}

		[HarmonyPostfix, HarmonyPatch(typeof(Player), "Import")]
		public static void Mecha_Import_Postfix(Player __instance)
		{
			__instance.package.SetSize(width.Value * rows);
			if (fuel.Value) {
				var reactor = __instance.mecha.reactorStorage;
				for (var i = 2; i < reactor.grids.Length; i++)
					if (reactor.grids[i].itemId != 0)
						__instance.TryAddItemToPackage(reactor.grids[i].itemId, reactor.grids[i].count, reactor.grids[i].inc, true);
				reactor.SetSize(2);
			}
		}

		[HarmonyPostfix, HarmonyPatch(typeof(Player), "SetForNewGame")]
		public static void Mecha_SetForNewGame_Postfix(Player __instance)
		{
			__instance.package.SetSize(width.Value * rows);
			if (fuel.Value)
				__instance.mecha.reactorStorage.SetSize(2);
		}

		[HarmonyPostfix, HarmonyPatch(typeof(UIGame), "OpenPlayerInventory")]
		public static void UIGame_OpenPlayerInventory_Postfix(UIGame __instance)
		{
			UIRoot.instance.uiGame.inventory.colCount = width.Value;
		}

		[HarmonyPostfix, HarmonyPatch(typeof(UIMechaWindow), "_OnOpen")]
		public static void UIMechaWindow_OnOpen_Postfix(UIMechaWindow __instance)
		{
			if (fuel.Value)
				UIRoot.instance.uiGame.mechaWindow.fuelGrid.colCount = 1;
		}

		[HarmonyPostfix, HarmonyPatch(typeof(GameHistoryData), "UnlockTechFunction")]
		public static void GameHistoryData_UnlockTechFunction_Postfix(int func)
		{
			if (func == 5) // Copied from DSPBigInventory, no idea how to figure out what's what here.
				GameMain.mainPlayer.package.SetSize(width.Value * rows);
		}

		[HarmonyPrefix, HarmonyPatch(typeof(UIAssemblerWindow), "OpenPlayerPackage")]
		public static bool UIAssemblerWindow_OpenPlayerPackage_Prefix(UIAssemblerWindow __instance)
		{

			if (!__instance.playerInventory.inited)
			{
				__instance.playerInventory.colCount = width.Value;
				__instance.playerInventory._Init(__instance.player.package);
				__instance.playerInventory._Open();
				__instance.windowTrans.sizeDelta = new Vector2((float)(width.Value * 50 + 80), (float)(rows * 50 + 220));
			}
			return false;

		}

		[HarmonyPrefix, HarmonyPatch(typeof(UIMinerWindow), "_OnOpen")]
		public static bool UIMinerWindow_OnOpen_Prefix(UIMinerWindow __instance)
		{
			if (GameMain.localPlanet != null && GameMain.localPlanet.factory != null)
			{
				__instance.playerInventory.colCount = width.Value;
				__instance.windowTrans.sizeDelta = new Vector2((float)(width.Value * 50 + 80), (float)(rows * 50 + 220));
			}
			return true;
		}
	}
}
